-- -----------------------------------------------------
-- Table `<db_name>`.`model`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`model` (
  `idmodel` INT NOT NULL AUTO_INCREMENT,
  `exec_name` VARCHAR(255) NOT NULL,
  `type` VARCHAR(3) NOT NULL,
  `version` VARCHAR(45) NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `date_created` DATETIME NOT NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`idmodel`),
  CONSTRAINT `fk_type_<model_name>`
    FOREIGN KEY (`type`)
    REFERENCES `<db_name>`.`model_type` (`type`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;