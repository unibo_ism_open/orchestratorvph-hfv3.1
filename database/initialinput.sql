-- -----------------------------------------------------
-- Table `<db_name>`.`initial_input`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`initial_input` (
  `idinitial_input` INT NOT NULL AUTO_INCREMENT,	
  `histology` INT NOT NULL,
  `TERT` INT NOT NULL,
  `ATRX` INT NOT NULL,
  `MYCN` INT NOT NULL,
  `ALT` INT NOT NULL,
  `ALK` INT NOT NULL,
  `gradiff` INT NOT NULL,
  `drug_regime` VARCHAR(50) NOT NULL,
  `names_list` VARCHAR(1000) NULL,
  `path` VARCHAR(1000) NULL,
  `datetime` DATETIME NOT NULL,
  `checksum` VARCHAR(400) NULL,
  PRIMARY KEY (`idinitial_input`)
  )
ENGINE = InnoDB;

