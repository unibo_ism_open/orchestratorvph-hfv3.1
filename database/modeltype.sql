-- -----------------------------------------------------
-- Table `<db_name>`.`model_type`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`model_type` (
  `type` VARCHAR(4) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `inputs` VARCHAR(510) NOT NULL,
  `inputfiles` VARCHAR(255) NOT NULL,
  `outputs` VARCHAR(255) NOT NULL,
  `outputfiles` VARCHAR(255) NOT NULL,
  `maxtime` INT NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `date_register` DATETIME NOT NULL,
  PRIMARY KEY (`type`))
ENGINE = InnoDB;