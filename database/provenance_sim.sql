-- -----------------------------------------------------
-- Table `<db_name>`.`provenance_sim`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`provenance_sim` (
  `idprov` INT NOT NULL AUTO_INCREMENT,
  `inputs_per_job` FLOAT NOT NULL,
  `nbins` INT NOT NULL,
  `nelems` INT NOT NULL,
  PRIMARY KEY (`idprov`),
  CONSTRAINT `fk_prov_sim`
    FOREIGN KEY (`idprov`)
    REFERENCES `<db_name>`.`simulation` (`idsimulation`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
