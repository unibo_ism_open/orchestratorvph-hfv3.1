-- -----------------------------------------------------
-- Table `<db_name>`.`relation_element_<idsim>`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`relation_element_<idsim>` (
  `idrelation_element` INT NOT NULL AUTO_INCREMENT,
  `id_orchestration` INT NOT NULL,
  `element_number` INT UNIQUE NOT NULL,
  `element_nodes` VARCHAR(45) NOT NULL,
  `interval_bins` INT NOT NULL,
  `is_sample` TINYINT NULL DEFAULT 0,
  `i_volume` DOUBLE NOT NULL,
  `i_oxygen` DOUBLE NOT NULL,
  `i_vascularization` DOUBLE NOT NULL,
  `i_cellularity_alive_NB` DOUBLE NOT NULL,
  `i_cellularity_alive_SC` DOUBLE NOT NULL,
  `i_cellularity_apoptotic_NB` DOUBLE NOT NULL,
  `i_cellularity_necrotic_NB` DOUBLE NOT NULL,
  `i_cellularity_apoptotic_SC` DOUBLE NOT NULL,
  `i_cellularity_necrotic_SC` DOUBLE NOT NULL,
  `i_cells_count` INT NULL,
  `volume` DOUBLE NULL,
  `volume_ratio` DOUBLE NULL,
  `oxygen` DOUBLE NULL,
  `cellularity_alive_NB` DOUBLE NULL,
  `cellularity_alive_SC` DOUBLE NULL,
  `cellularity_apoptotic_NB` DOUBLE NULL,
  `cellularity_necrotic_NB` DOUBLE NULL,
  `cellularity_apoptotic_SC` DOUBLE NULL,
  `cellularity_necrotic_SC` DOUBLE NULL,
  `vegf` DOUBLE NULL,
  `mean_telomer_NB` DOUBLE NULL,
  `std_telomer_NB` DOUBLE NULL,
  `mean_telomer_SC` DOUBLE NULL,
  `std_telomer_SC` DOUBLE NULL,
  `mean_apoptotic_NB` DOUBLE NULL,
  `std_apoptotic_NB` DOUBLE NULL,
  `mean_necrotic_NB` DOUBLE NULL,
  `std_necrotic_NB` DOUBLE NULL,
  `mean_apoptotic_SC` DOUBLE NULL,
  `std_apoptotic_SC` DOUBLE NULL,
  `mean_necrotic_SC` DOUBLE NULL,
  `std_necrotic_SC` DOUBLE NULL,
  `mean_diff_NB` DOUBLE NULL,
  `std_diff_NB` DOUBLE NULL,
  `cells_count` INT NULL,
  `provenance_sim` INT NULL,
  PRIMARY KEY (`idrelation_element`, `id_orchestration`),
  CONSTRAINT `fk_relation_element_<output_table><idsim>`
    FOREIGN KEY (`provenance_sim`)
    REFERENCES `<db_name>`.`<output_table>` (`id_simulation`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_relation_element_orchestration<idsim>`
    FOREIGN KEY (`id_orchestration`)
    REFERENCES `<db_name>`.`orchestration` (`idorchestration`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
