-- -----------------------------------------------------
-- Table `<db_name>`.`final_output`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`final_output` (
  `idfinal_output` INT NOT NULL AUTO_INCREMENT,
  `names_list` VARCHAR(1000) NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `datetime` DATETIME NOT NULL,
  `checksum` VARCHAR(400) NULL,
  `id_initial_input` INT NOT NULL,
  PRIMARY KEY (`idfinal_output`),
  CONSTRAINT `fk_final_output_initial_input`
    FOREIGN KEY (`id_initial_input`)
    REFERENCES `<db_name>`.`initial_input` (`idinitial_input`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
