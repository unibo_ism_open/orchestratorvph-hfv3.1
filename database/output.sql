-- -----------------------------------------------------
-- Table `<db_name>`.`output_<model_name>`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`output_<model_name>` (
  `idoutput_<model_name>` INT NOT NULL AUTO_INCREMENT,
  `names_list` VARCHAR(1000) NULL,
  `path` VARCHAR(1000) NOT NULL,
  `datetime` DATETIME NOT NULL,
  `id_input_<model_name>` INT NOT NULL UNIQUE,
  `id_simulation` INT NOT NULL,
  PRIMARY KEY (`idoutput_<model_name>`),
  CONSTRAINT `fk_output_simulation_<model_name>`
    FOREIGN KEY (`id_simulation`)
    REFERENCES `<db_name>`.`simulation` (`idsimulation`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_output_<model_name>`
    FOREIGN KEY (`id_input_<model_name>`)
    REFERENCES `<db_name>`.`input_<model_name>` (`idinput_<model_name>`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
