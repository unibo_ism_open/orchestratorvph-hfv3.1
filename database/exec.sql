-- -----------------------------------------------------
-- Table `<db_name>`.`exec`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`exec_table` (
  `idexec_table` INT NOT NULL AUTO_INCREMENT,
  `table_name` VARCHAR(45) NOT NULL,
  `datetime` DATETIME NOT NULL,
  `provenance_id` INT NOT NULL,
  `action` VARCHAR(10) NOT NULL DEFAULT 'insert',
  PRIMARY KEY (`idexec_table`))
ENGINE = InnoDB;