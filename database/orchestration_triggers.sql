## Update some orchestration fields after insert final output
DROP TRIGGER IF EXISTS updateOrchestration 
CREATE TRIGGER updateOrchestration AFTER INSERT ON final_output
FOR EACH ROW
BEGIN
	DECLARE inputdatetime DATETIME;
    SET inputdatetime = (SELECT `datetime` FROM initial_input WHERE `idinitial_input` = NEW.`id_initial_input`);
	UPDATE orchestration
    SET `runtime` = TIMEDIFF(NEW.`datetime`, inputdatetime) WHERE `id_initial_input` = NEW.`id_initial_input`;
    UPDATE orchestration
    SET `id_final_output` = NEW.`idfinal_output` WHERE `id_initial_input` = NEW.`id_initial_input`;
END