-- -----------------------------------------------------
-- Table `<db_name>`.`input_<model_name>`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`input_<model_name>` (
  `idinput_<model_name>` INT NOT NULL AUTO_INCREMENT,
  `names_list` VARCHAR(1000) NULL,
  `path` VARCHAR(1000) NOT NULL,
  `datetime` DATETIME NOT NULL,
  `checksum` VARCHAR(400) NULL,
  `provenance_sim` INT NULL,
  `id_simulation` INT NULL,
  PRIMARY KEY (`idinput_<model_name>`),
  CONSTRAINT `fk_simulation_<model_name>`
    FOREIGN KEY (`id_simulation`)
    REFERENCES `<db_name>`.`simulation` (`idsimulation`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prov_sim_<model_name>`
    FOREIGN KEY (`provenance_sim`)
    REFERENCES `<db_name>`.`simulation` (`idsimulation`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
