INSERT IGNORE INTO model VALUES(1, "mock_thermal3.py", "O1", 1.0, "/../../Models/", "UNIZAR", "2020-11-01", "Mock oxygen transport model");
INSERT IGNORE INTO model VALUES(2, "mock_cellular2.py", "C1", 1.0, "/../../Models/", "CHMT", "2022-01-01", "Mock cell model");
INSERT IGNORE INTO model VALUES(3, "mock_FGPU_NB2_determ2.py", "A1" ,1.0, "/../../Models/", "USFD", "2020-11-01", "Mock ABM model");
INSERT IGNORE INTO model VALUES(4, "mock_structural2.py", "M1", 1.0, "/../../Models/", "UNIZAR", "2020-11-01", "Mock mechanical model");
INSERT IGNORE INTO model VALUES(5, "thermal_primage.inp", "O1", 2.4, "/../../Models/", "UNIZAR", "2021-09-20", "Oxygen transport model");
INSERT IGNORE INTO model VALUES(6, "SubcellularModel.py", "C1", 1.3, "/../../Models/", "CHMT", "2022-01-10", "Cell model");
INSERT IGNORE INTO model VALUES(7, "FGPU1933f", "A1", 13.5, "/../../Models/", "USFD", "2022-01-10", "ABM model");
INSERT IGNORE INTO model VALUES(8, "structural_primage.inp", "M1", 2.6, "/../../Models/", "UNIZAR", "2021-09-20", "Mechanical model");
