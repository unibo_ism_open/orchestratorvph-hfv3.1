-- -----------------------------------------------------
-- Table `<db_name>`.`simulation`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`simulation` (
  `idsimulation` INT NOT NULL AUTO_INCREMENT,
  `id_orchestration` INT NOT NULL,
  `description` VARCHAR(255) NULL,
  `datetime` DATETIME NOT NULL,
  `runtime` TIME NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `id_model` INT NOT NULL,
  `loop` INT NOT NULL,
  `nelems` INT NOT NULL,
  PRIMARY KEY (`idsimulation`, `id_orchestration`),
  CONSTRAINT `fk_simulation_model`
    FOREIGN KEY (`id_model`)
    REFERENCES `<db_name>`.`model` (`idmodel`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_simulation_orchestration`
    FOREIGN KEY (`id_orchestration`)
    REFERENCES `<db_name>`.`orchestration` (`idorchestration`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
