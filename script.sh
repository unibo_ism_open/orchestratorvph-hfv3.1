#!/bin/bash -l
#SBATCH -J primage_3.0_test
#SBATCH -N 1 
#SBATCH --ntasks-per-node=4
#SBATCH --mem-per-cpu=2000MB
#SBATCH --time=168:00:00
#SBATCH -A plgprimage4-cpu
#SBATCH -p plgrid-services
#SBATCH --output="output.out"
#SBATCH --error="error.err"
cd src/
module load python/3.8.2-gcccore-9.3.0
pip3 install --upgrade pip
pip3 install pandas
pip3 install mysql-connector-python
python3 -u master.py

