import os
import sys
import json
import random
import subprocess
import time as timelib

class A1:
    
    def __init__(self, SQL, name, cluster, modelspath=""):
        
        ## CLASS VARIABLES
        ## to print
        self.debug = True
        
        ## SQL connection
        self.SQL = SQL
        
        ## Name
        self.name = name
        
        ## Cluster
        self.cluster = cluster

        ## Models path
        self.modelspath = modelspath
        
        ## set proxy
        if cluster == "ares":
            self.proxy = os.environ["PROXY"]
        
        ## Get columns to access on DB and the filenames to serialize
        query = f"SELECT inputs, inputfiles, outputs, outputfiles FROM {self.SQL.mydb.database}.model_type WHERE type = \"A1\""
        result = self.SQL.mysql_execute(query)
        self.columns = result[0][0].split(', ')
        self.inputfiles = result[0][1].split(', ')
        self.outputs = result[0][2].split(', ')
        self.outputfiles = result[0][3].split(', ')
                
    
    def serializer(self, path, idinput, datatoseri, loop):
        
        ## Get data
        cells = datatoseri[0:6]
        cellscount = datatoseri[6]
        oxy = datatoseri[7]
        vol = datatoseri[8]  
        telo = datatoseri[9:13]
        death = datatoseri[13:21]
        diff = datatoseri[21:23]
        start = datatoseri[23].replace('\n',',')
        end = datatoseri[24].replace('\n',',')
        effects = datatoseri[25].split('\n')
        effectsstring = []
        startstring = []
        endstring = []
        
        ## prepare effects list
        for k in range(len(start.split(','))):
            for i in range(len(effects)):
                value = effects[i].split()[1]
                effectsstring.append(float(value))   
                
        ## prepare start and end lists
        start = start.split(',')
        end = end.split(',')
        for i in range(len(start)):
            startstring.append(int(start[i]))
            endstring.append(int(end[i]))
                 

        ## create input file
        filename = "input-"+idinput+".json"      

        ## copy calibration file
        if self.cluster != "local":
            pathcali = f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/'
        else:
            pathcali = '../'
        cmd = f'cp {pathcali}Models_examples/abm/calibration.json {path}calibration.json'
        os.system(cmd)
        
        ## read template file
        ## copy calibration file
        if self.cluster != "local":
            pathtempl = f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/'
        else:
            pathtempl = '../'
        template = json.load(open(f'{pathtempl}/Models_examples/abm/USFD_sample_input.json'))

        ## serialize data based on template file
        template["config"]["seed"] = int(random.uniform(2,999999))
        template["config"]["environment"]["O2"] = round(oxy,6)
        template["config"]["environment"]["V_tumour"] = round(vol,6)
        template["config"]["environment"]["orchestrator_time"] = loop
        template["config"]["environment"]["cellularity"] = cells
        template["config"]["environment"]["cell_count"] = cellscount
        template["config"]["environment"]["nb_telomere_length_mean"] = telo[0]
        template["config"]["environment"]["nb_telomere_length_sd"] = telo[1]
        template["config"]["environment"]["sc_telomere_length_mean"] = telo[2]
        template["config"]["environment"]["sc_telomere_length_sd"] = telo[3]
        template["config"]["environment"]["extent_of_differentiation_mean"] = diff[0]
        template["config"]["environment"]["extent_of_differentiation_sd"] = diff[1]
        template["config"]["environment"]["nb_apop_signal_mean"] = death[0]
        template["config"]["environment"]["nb_apop_signal_sd"] = death[1]
        template["config"]["environment"]["sc_apop_signal_mean"] = death[2]
        template["config"]["environment"]["sc_apop_signal_sd"] = death[3]
        template["config"]["environment"]["nb_necro_signal_mean"] = death[4]
        template["config"]["environment"]["nb_necro_signal_sd"] = death[5]
        template["config"]["environment"]["sc_necro_signal_mean"] = death[6]
        template["config"]["environment"]["sc_necro_signal_sd"] = death[7]
        template["config"]["environment"]["drug_effects"] = effectsstring
        template["config"]["environment"]["start_effects"] = startstring
        template["config"]["environment"]["end_effects"] = endstring
             
        json_text = json.dumps(template, indent=4)

        f = open(path+filename,'w')
        f.write(json_text)
        f.close()

        return [filename]
    
    
    def callModel(self, path, idinputs, idmodel, memory = 5, time = 0.5, description = "A1 model"):

        ## Get model filename
        query = f"SELECT exec_name FROM {self.SQL.mydb.database}.model WHERE idmodel = {str(idmodel)}"
        self.model = self.SQL.mysql_execute(query)[0][0]

        if type(idinputs) == str:## If is a re-call
            idinputs = idinputs.split()
        idcall = idinputs[0]
    
        ## Create an array of the ids called
        ids = ""
        for i in idinputs:
            ids += str(i)+" "

        ## job = path, id log file, list of ids the job is running, time of creation, name of logfile
        joberrorpath = path+"error_"+str(idcall)+".err"
        joblog = "log_"+str(idcall)+".out"
        joblogpath = path+joblog
        
        ##set copy model's name
        newmodel = self.model+"_"+str(idcall)
        ## Copy the model (both with the same name) to the directory of I/O results 
        cmd = f"cp {self.modelspath}{self.model} {path}{newmodel}"
        os.system(cmd)

        ## calls thermal model
        if self.cluster == "ares":

            ## set dictionaries
            backend_conf = "'{\"ENDPOINT\":\"https://submit.plgrid.pl\",\"PROXY\":\""+self.proxy+"\"}'"
            job_conf = ("'{\"script\":\"#!/bin/bash \\n#SBATCH -J ABM_models \\n#SBATCH -N 1 \\n#SBATCH --ntasks-per-node=1 \\n#SBATCH -A plgprimage4-gpu \\n"
            "#SBATCH --mem-per-cpu="+str(memory)+"GB \\n#SBATCH --time=00:"+str(time)+":00 \\n#SBATCH -p plgrid-gpu-v100\\n#SBATCH --gres=gpu:1\\n"
            f"#SBATCH --error={joberrorpath} \\n#SBATCH --output={joblogpath}\\n"
            f"cd {path} \\necho $SLURM_JOBID \\nmodule load cmake/3.20.1-gcccore-10.3.0\\nmodule load cudacore/11.2.2\\n"
            f"nvidia-smi \\nfor id in {ids}\\ndo \\n./{newmodel} --in input-$id.json --primage output-$id.json\\ndone \\necho {ids}\\necho Done.\","
            "\"host\":\"login01.ares.cyfronet.pl\"}'")

            ## Calls rimrock
            cmd = f'python3 {os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/Models/hpc-connector/hpc-connector.py --backend Prometheus --backend-conf {backend_conf} job --job-config {job_conf} submit' 

        if self.cluster == "local":
            ## copy files
            cmd = f'cp ../Models_examples/abm/IOs.csv {path};cp ../Models_examples/abm/USFD_sample_output.json {path}'
            os.system(cmd)
            cmd = f"cd {path}; python3 {newmodel} \"{ids}\" {idcall}"
            #cmd = f'rm {path}/IOs.csv;rm {path}/USFD_sample_input.json'
            #os.system(cmd)

        try:
            os.system(cmd)
        except:
            sys.exit(" FAILED.")            

        t_start = timelib.perf_counter()
        ## Register the path and the id of script input in the list of jobs running
        user = os.environ["USER"]
        self.SQL.mysql_insert("job", "modelfile, errorfile, logfile, status, idinputs, author, dt_creation, runtime, id_model, memory, time, description", 
        f"\"{newmodel}\", \"{joberrorpath}\", \"{joblogpath}\", \"pending\", \"{ids}\", \"{user}\", NOW(), {t_start}, {idmodel}, {memory}, {time}, \"{description}\"")
    
        ## Writes path on the output.out file to emulate cluster
        if self.cluster == "local":
            outputfile = open("../output.out",'w')
            outputfile.write("""Submitting job....
            Job submmited succesfully
            Job ID: 12345.login01.prometheus.cyfronet.pl
            State: PENDING
            Information: {"time": "2022-06-09T16:19:03.637905", "data": {"job_id": "12345.login01.prometheus.cyfronet.pl", 
            "stdout_path": "https://data.plgrid.pl/download//prometheus/net/people/plgvarella/vph-hfv3-2.0/src/../../../../"""+path+""",
            "stderr_path": "https://data.plgrid.pl/download//prometheus/net/people/plgvarella/vph-hfv3-2.0/src/../../../../scratch/people/plgvarella/iofiles_v2.0_sphere/Orchestration_1/Loop_1/oxygen_transport_2435/error.err", "status": "QUEUED"}}
            Job called with success!
            """)
            outputfile.close() 


    def deserializer(self, idslist, success):

        for idinput in idslist:
            
            idinput = str(idinput)
            ## gets id simulation (by idinput)
            query = f"SELECT id_simulation, path FROM {self.SQL.mydb.database}.input_{self.name} WHERE idinput_{self.name} = {idinput};"
            results = self.SQL.mysql_execute(query)
            idsim = str(results[0][0])
            path = results[0][1]

            ## writes on db
            ## If has specific columns
            query = ""
            if success == True:
                f = f'output-{idinput}.json'
                if os.path.isfile(path+f) == True:
                    query = f'INSERT INTO {self.SQL.mydb.database}.output_{self.name} VALUES (NULL, \"{f}\", \"{path}\", NOW(),{idinput}, {idsim});'
                ## If execution failed, try again
                else:
                    ## Start the stopwatch / counter 
                    print(f'model {self.name} ID {idinput} has failed.')
                    ## Get idmodel
                    query = f'SELECT id_model FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim};'
                    idmodel = self.SQL.mysql_execute(query)[0][0]
                    self.callModel(path, idinput, idmodel)
                    timelib.sleep(10)
                    print(f'model {self.name} ID {idinput} was called again.')
                    return 2
            else: ## Insert output without file
                query = f'INSERT INTO {self.SQL.mydb.database}.output_{self.name} VALUES (NULL, NULL, \"{path}\", NOW(),{idinput}, {idsim});'

            ## Execute insertion query
            try:
                self.SQL.mysql_execute_NOreturn(query)
            except:
                print(f'ABM ID {idinput} is inserted.')

            ## Updates simulation status
            query = f'SELECT `datetime` FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim};'
            datetime = self.SQL.mysql_execute(query)[0][0]
            query = f'UPDATE {self.SQL.mydb.database}.simulation SET status = \"Finished\", runtime = TIMEDIFF(NOW(),\'{str(datetime)}\') WHERE idsimulation = {idsim};'
            self.SQL.mysql_execute_NOreturn(query)
                
        return 0


    ## Try to identify error and recal the model if possible to finish execution
    def checkStatus(self, cluster, idjob, idcluster, errorpath):

        ## Local path
        path = errorpath.split('error')[0]
        
        ## Check for job status
        if cluster == "ares": 
            ##Check job status
            cmd = f"sacct -j {idcluster} --format=\"State\""
            status = subprocess.check_output(cmd, shell=True)
            status = status.decode()
            ## If finished 
            ## ANSYS models always finish with failed status (HPC bug) 
        if cluster == "local":
            jobinfo = open(path+"jobinfo.txt", 'r')
            status = jobinfo.read()                              
        
        query = f'SELECT id_model, idinputs, memory, time, logfile FROM {self.SQL.mydb.database}.job WHERE id = {idjob}'
        results = self.SQL.mysql_execute(query)
        idmodel = results[0][0]
        idinputs = results[0][1]
        memory = results[0][2]
        time = results[0][3]
        logfilepath = results[0][4]
        
        if status.find("ERROR") != -1:
            return self.returnError(id)
        
        if status.find("TIMEOUT") != -1:
            ## increases time
            newtime = int(time)*2
            ## update job table
            query = f'UPDATE {self.SQL.mydb.database}.job SET status = "timeout" WHERE id = {idjob}'
            self.SQL.mysql_execute_NOreturn(query)
            ## Remove error and log files
            cmd = f"rm {errorpath} {logfilepath}"
            os.system(cmd)
            ## recall job
            self.callModel(path, idinputs, idmodel, memory, newtime, description="A1 model re-call")
            print(f"The A1 job {idcluster} was restarted for timeout reason.")
            timelib.sleep(10)
            return "RE-CALLED", idinputs
        
        if status.find("OUT_OF_ME") != -1:
            ## Increase memory
            newmem = int(memory)*2
            ## Update job table
            query = f'UPDATE {self.SQL.mydb.database}.job SET status = "out of memory" WHERE id = {idjob}'
            self.SQL.mysql_execute_NOreturn(query)
            ## Remove error and log files
            cmd = f"rm {errorpath} {logfilepath}"
            os.system(cmd)
            ## recall job
            self.callModel(path, idinputs, idmodel, newmem, time, description="A1 model re-call")
            print(f"A1 job {idcluster} was restarted for out of memory reason.")
            timelib.sleep(10)
            return "RE-CALLED", idinputs
        
        if status.find("CANCELLED") != -1: ## cancelled 
            print(f"A1 job {idcluster} was cancelled by the user.")
            return "CANCELLED", idinputs
        
        if status.find("COMPLETED") != -1 and status.find("RUNNING") == -1:
            ## Get idinputs
            inputs = idinputs.split()
            ## Check if output files exist
            listfiles = os.listdir(path)
            ## boolean if outputs exist
            ## update idslist in job table
            idsfinished = []
            for i in range(len(inputs)):
                output = f"output-{inputs[i]}.json"
                exist = False
                for f in listfiles:
                    if f == output:
                        exist = True
                        idsfinished.append(inputs[i])
                        break
                if exist == False:
                    print("MISSING "+output+" in folder "+path)
                    self.deserializer([inputs[i]], False)

            return "COMPLETED", idsfinished
        
        if status.find("RUNNING") == -1: ## If not completed, not failed, not running then unknown status
            return "RUNNING", idinputs

        return status, idinputs


    def returnError(self, id):
        
        ## update job table
        query = f'UPDATE {self.SQL.mydb.database}.job SET status = "ERROR" WHERE id = {id}'
        self.SQL.mysql_execute_NOreturn(query)
        print(f"A1 job {id} has an error.")
        
        return "ERROR", id
