import os
import sys
import time as timelib
import subprocess

class O1:
    
    def __init__(self, SQL, name, cluster, modelspath=""):
        
        ## CLASS VARIABLES
        ## to print
        self.debug = True
        
        ## SQL connection
        self.SQL = SQL

        ## Name
        self.name = name
        
        ## Cluster
        self.cluster = cluster

        ## Models path
        self.modelspath = modelspath
        
        ## set proxy
        if cluster == "ares":
            self.proxy = os.environ["PROXY"]
        
        ## Get columns to access on DB and the filenames to serialize
        query = f"SELECT inputs, inputfiles, outputs, outputfiles FROM {self.SQL.mydb.database}.model_type WHERE type = \"O1\""
        result = self.SQL.mysql_execute(query)
        self.columns = result[0][0].split(', ')
        self.inputfiles = result[0][1].split(', ')
        self.outputs = result[0][2].split(', ')
        self.outputfiles = result[0][3].split(', ')
                
    
    ## Serialize the inputs and creates a folder to store them
    def serializer(self, path, datatoseri):

        ## filenames that will be written
        filenames = self.inputfiles

        ## Gets data 
        elements = datatoseri[0]
        nodes = datatoseri[1]
        cellu = datatoseri[-2]
        vasc = datatoseri[-1]

        if elements == "" or nodes == "" or cellu == "" or vasc == "":
            return ""
        
        ## Process strings
        elements = elements.replace(", ", "")
        nodes = nodes.replace(", ", "")
        cellu = cellu.replace(", ", "")
        vasc = vasc.replace(", ", "")

        ## Creates the files
        felements = open(path+filenames[0],'w')
        fnodes = open(path+filenames[1],'w')
        fcellu = open(path+filenames[-2],'w')
        fvasc = open(path+filenames[-1],'w')

        ## Prepare strings
        strelements = self.SQL.liststostring(elements)
        strnodes = self.SQL.liststostring(nodes)
        strcellu = self.SQL.liststostring(cellu)
        strvascu = self.SQL.liststostring(vasc)
        
        ## Drop last '\n'
        strelements = strelements[0:-1]
        strnodes = strnodes[0:-1]
        strcellu = strcellu[0:-1]
        strvascu = strvascu[0:-1]
        
        ## Write files
        felements.write(strelements)
        fnodes.write(strnodes)
        fcellu.write(strcellu)
        fvasc.write(strvascu)

        ## Closes files
        felements.close()
        fnodes.close()
        fcellu.close()
        fvasc.close()

        return filenames
    
    
    def callModel(self, path, idinput, idmodel, idsim = -1, memory = 900 , time = 5, description = "O1 model"): ## memory in GB and time in hours ##idsim for the cases other parameters are needed
        
        ## Get model filename
        query = f"SELECT exec_name FROM {self.SQL.mydb.database}.model WHERE idmodel = {str(idmodel)};"
        self.model = self.SQL.mysql_execute(query)[0][0]
        
        ## job = path, id log file, list of ids the job is running, time of creation, name of logfile
        joberrorpath = path+"error_"+str(idinput)+".err"
        joblog = "log_"+str(idinput)+".out"
        joblogpath = path+joblog
        listid = str(idinput)

        ##set copy model's name
        newmodel = self.model+"_"+str(idinput)
        ## Copy the model (both with the same name) and scritp to the directory of I/O results 
        cmd = f"cp {self.modelspath}{self.model} {path}{newmodel}"
        os.system(cmd)
        cmd = f"cp {self.modelspath}elemental2nodal.py {path}"
        os.system(cmd)
        
        ## calls thermal model
        if self.cluster == "ares":
            ## set dictionaries
            backend_conf = "'{\"ENDPOINT\":\"https://submit.plgrid.pl\",\"PROXY\":\""+self.proxy+"\"}'"
            job_conf = ("'{\"script\":\"#!/bin/bash \\n#SBATCH -J diffusionAres \\n#SBATCH -N 1\\n#SBATCH --ntasks-per-node=8\\n#SBATCH -A plgprimage4-cpu \\n"
            "#SBATCH -p plgrid\\n#SBATCH --mem-per-cpu="+str(memory)+"MB \\n#SBATCH --time="+str(time)+":00:00 \\n#SBATCH --error="+joberrorpath+"\\n"
            "#SBATCH --output="+joblogpath+"\\ncd "+path+" \\necho $SLURM_JOBID \\nmodule load .plgrid \\nmodule load plgrid/libs/python-numpy/2021.05-intel-2021a \\n" 
            "python3 elemental2nodal.py > stdnodal.out \\nmodule load motif/2.3.8-gcccore-10.3.0 \\n"
            "module add ansys/2022R1\\n export I_MPI_FALLBACK=1\\n export I_MPI_FABRICS=\\n export KMP_AFFINITY=disabled\\n"
            "ansys221 -b -i "+newmodel+" -o solver_oxygen_transport.out -np 8\\necho Done.\", \"host\":\"login01.ares.cyfronet.pl\"}'")

            ## Calls rimrock
            cmd = f'python3 {os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/Models/hpc-connector/hpc-connector.py --backend Prometheus --backend-conf {backend_conf} job --job-config {job_conf} submit' 

        if self.cluster == "local":
            cmd = "cd "+path+"; python3 elemental2nodal.py > stdnodal.out; python3 "+newmodel+" "+str(idinput)
        try:
            os.system(cmd)
        except:
            sys.exit(cmd+" FAILED.")

        ## Register the path and the id of script input in the list of jobs running
        t_start = timelib.perf_counter()
        user = os.environ["USER"]
        self.SQL.mysql_insert("job", "modelfile, errorfile, logfile, status, idinputs, author, dt_creation, runtime, id_model, memory, time, description", f"\"{newmodel}\", \"{joberrorpath}\", \"{joblogpath}\", \"pending\", \"{listid}\", \"{user}\", NOW(), {t_start}, {idmodel}, {memory}, {time}, \"{description}\"")
        
        ## Writes path on the output.out file to emulate cluster
        if self.cluster == "local":
            outputfile = open("../output.out",'w')
            outputfile.write("""Submitting job....
            Job submmited succesfully
            Job ID: 12345.login01.prometheus.cyfronet.pl
            State: PENDING
            Information: {"time": "2022-06-09O16:19:03.637905", "data": {"job_id": "12345.login01.prometheus.cyfronet.pl", 
            "stdout_path": "https://data.plgrid.pl/download//prometheus/net/people/plgvarella/vph-hfv3-2.0/src/../../../../"""+path+""",
            "stderr_path": "https://data.plgrid.pl/download//prometheus/net/people/plgvarella/vph-hfv3-2.0/src/../../../../scratch/people/plgvarella/iofiles_v2.0_sphere/Orchestration_1/Loop_1/oxygen_transport_2435/error.err", "status": "QUEUED"}}
            Job called with success!
            """)
            outputfile.close() 
        
    
    def deserializer(self, idslist, success):

        for idinput in idslist:

            idinput = str(idinput)
            ## gets id simulation (by idinput)
            query = f"SELECT id_simulation, path FROM {self.SQL.mydb.database}.input_{self.name} WHERE idinput_{self.name} = {idinput}"
            results = self.SQL.mysql_execute(query)
            idsim = str(results[0][0])
            path = results[0][1]

            if success == True:
                ## writes on db
                ## If has specific columns
                names = self.SQL.listtostring(self.outputfiles, True)
                query = f"INSERT INTO {self.SQL.mydb.database}.output_{self.name} VALUES (NULL, \""+names+"\", \""+path+"\", NOW(), "+str(idinput)+", "+idsim+");"
            else:
                query = f"INSERT INTO {self.SQL.mydb.database}.output_{self.name} VALUES (NULL, NULL, \""+path+"\", NOW(), "+str(idinput)+", "+idsim+");"

            ## Execute insertion query
            try:
                self.SQL.mysql_execute_NOreturn(query)
            except:
                print(f'query {query} failed.')
                return 1

            ## updates simulation status
            query = f'SELECT `datetime`, id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim};'
            datetime = self.SQL.mysql_execute(query)[0][0]
            idorch = self.SQL.mysql_execute(query)[0][1]
            if success == True:
                query = f'UPDATE {self.SQL.mydb.database}.simulation SET status = \"Finished\", runtime = TIMEDIFF(NOW(),\'{str(datetime)}\') WHERE idsimulation = {idsim};'
            else: ## update orchestration too
                query = f'UPDATE {self.SQL.mydb.database}.simulation SET status = \"Failed\", runtime = TIMEDIFF(NOW(),\'{str(datetime)}\') WHERE idsimulation = {idsim};'
                self.SQL.mysql_execute_NOreturn(query)
                query = f'UPDATE {self.SQL.mydb.database}.orchestration SET status = \"Failed\", runtime = TIMEDIFF(NOW(),\'{str(datetime)}\') WHERE idorchestration = {idorch};'
                self.SQL.mysql_execute_NOreturn(query)
                

        print(f"O1 id {self.SQL.listtostring(idslist)} serialized.")
        return 0


    ## Try to identify error and recal the model if possible to finish execution
    def checkStatus(self, cluster, idjob, idcluster, errorpath):

        ## Local path
        path = errorpath.split('error')[0]
        
        ## Check for job status
        if cluster == "ares": 
            ##Check job status
            cmd = f"sacct -j {idcluster} --format=\"State\""
            status = subprocess.check_output(cmd, shell=True)
            status = status.decode()
            ## If finished 
            ## ANSYS models always finish with failed status (HPC bug) 
        if cluster == "local":
            jobinfo = open(path+"jobinfo.txt", 'r')
            status = jobinfo.read()

        query = f'SELECT id_model, idinputs, memory, time, logfile FROM {self.SQL.mydb.database}.job WHERE id = {idjob}'
        results = self.SQL.mysql_execute(query)
        idmodel = results[0][0]
        idinput = results[0][1]
        memory = results[0][2]
        time = results[0][3]
        logfilepath = results[0][4]
        file0path = path+"solver_oxygen_transport.out"
        file0 = open(file0path, 'r')
        lines = file0.read()
            
        if status.find("RUNNING") == -1: ##if it is not running
            
            if status.find("COMPLETED") != -1:
                print("JOB FINISHED!!!")
                ## verify if all output files exist
                for output in self.outputfiles:
                    listfiles = os.listdir(path)
                    exist = False
                    for f in listfiles:
                        if f == output:
                            exist = True
                            break
                    if exist == False:
                        print("MISSING "+output+" in folder "+path)
                        return "ERROR", idinput
                    #if lines.find("ERROR") != -1: ## If some error was reported, break
                    #    print("Simulation failed for error. path:"+path)
                    #    return "ERROR", idinput
                    oxyfile = open(path+"oxygen_i.txt",'r')
                    oxylines = oxyfile.readlines()
                    ## Check if all values are smaller than 1.25
                    for l in oxylines:
                        if float(l) > 1.25:
                            print("oxygen value too high was found.")
                            return "ERROR", idinput
                    
                return "COMPLETED", idinput                                
            else:    
                if status.find("TIMEOUT") != -1:
                    print(idjob)
                    ## increases time
                    newtime = int(time)*2
                    ## update job table
                    query = f'UPDATE {self.SQL.mydb.database}.job SET status = "timeout" WHERE id = {idjob}'
                    self.SQL.mysql_execute_NOreturn(query)
                    ## Remove error and log files
                    cmd = f"cd {path}; error* log* solver* file* cleanup* std* *.CDB"
                    os.system(cmd)
                    ## recall job
                    self.callModel(path, idinput, idmodel, memory=memory, time=newtime, description="O1 model re-call")
                    print(f"The O1 job {idcluster} was restarted for timeout reason.")
                    timelib.sleep(10)
                    return "RE-CALLED", idinput
                else:
                    if status.find("OUT_OF_ME") != -1:
                        newmem = int(memory)*2
                        query = f'UPDATE {self.SQL.mydb.database}.job SET status = "out of memory" WHERE id = {idjob}'
                        self.SQL.mysql_execute_NOreturn(query)
                        # Start the stopwatch / counter 
                        t_start = timelib.perf_counter()  
                        ## Remove error and log files
                        cmd = f"cd {path}; error* log* solver* file* cleanup* std* *.CDB"
                        os.system(cmd)
                        ## recall job
                        self.callModel(path, idinput, idmodel, memory=newmem, time=time, description="O1 model re-call")
                        print(f"O1 job {idcluster} was restarted for out of memory reason.")
                        timelib.sleep(10)
                        return "RE-CALLED", idinput
                    else:
                        if status.find("CANCELLED+") != -1: ## cancelled
                            print(f"O1 job {idcluster} was cancelled by the user.")
                            return "CANCELLED", idinput
                        else:
                            if status.find("ERROR") != -1 or status.find("FAILED") != -1:
                                if lines.find("ERROR") != -1: ## if failed
                                    return self.returnError(id)
                            else: ##unknown status
                                return "Unknown status", idinput
        else:
            return "RUNNING", idinput
                
                            
    def returnError(self, id):
        
        ## update job table
        query = f'UPDATE {self.SQL.mydb.database}.job SET status = "timeout" WHERE id = {id}'
        self.SQL.mysql_execute_NOreturn(query)
        print(f"O1 job {id} has an error.")
        
        return "ERROR"
                                

    
                                
                
                

