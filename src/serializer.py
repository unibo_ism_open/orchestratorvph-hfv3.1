import os
import sys
import time
from typing import ValuesView
import O1 as O1
import A1 as A1
import C1 as C1
import M1 as M1
import subprocess
import pythonSQL as pysql

class serializer:

    def __init__(self, dbname, name, idmodel, cluster, iofolder, modelspath, pwd, poll_time):

        ## Model name (for prints)
        self.name = name
        
        ## Creates a instance of SQLpython class with a new database connection 
        self.SQL = pysql.SQLpython(cluster, pwd, False, dbname)

        ## Get name and ID of the model
        m = f"SELECT exec_name, type FROM {self.SQL.mydb.database}.model WHERE idmodel ={str(idmodel)}; "
        m = self.SQL.mysql_execute(m)
        if m != []:
            self.model = m[0][0]
            self.idmodel = idmodel
            self.type = m[0][1]
        else:
            sys.exit("Failed to get info of the model ID "+str(self.idmodel)+".")

        ## Stopping criterion 
        self.finished = False

        ## Time variables
        self.total_process = 0
        self.polling_process = 0

        ## Set the interval in seconds to polling
        self.poll_time = poll_time

        ## Set the name of I/O folder
        self.iofolder = iofolder

        ## table name to poll
        self.poll_table = 'input_'+name
        ## table name to write inputs
        self.write_table = 'input_'+name
        
        ## Create instance of model type class
        if self.type == "O1":
            self.tyclass = O1.O1(self.SQL, name, cluster)
        if self.type == "C1":
            self.tyclass = C1.C1(self.SQL, name, cluster)
        if self.type == "A1":
            self.tyclass = A1.A1(self.SQL, name, cluster)
        if self.type == "M1":
            self.tyclass = M1.M1(self.SQL, name, cluster, modelspath)

        ## Verify if there are inputs interrupteds
        query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.write_table} WHERE checksum IS NULL;"
        interrupteds = self.SQL.mysql_execute(query)
        ## For each interrupted, write on exec_table
        for interrupted in interrupteds:
            query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(interrupted[0])};"
            id = self.SQL.mysql_execute(query)[0][0]
            columns = f"`table_name`, `datetime`, `provenance_id`, `action`"
            values = f"\"{self.poll_table}\", NOW(), {id}, 'insert'"
            self.SQL.mysql_insert("exec_table", columns, values)


    ## Waits a new insertion on 'exec_table'
    def wait(self):
        
        while self.finished == False:

            ## process time
            process_start_p = time.time() 

            ## Pools the 'exec_table'
            query = f"SELECT * FROM {self.SQL.mydb.database}.exec_table WHERE `table_name` = \"{self.poll_table}\" AND action = \"insert\";"
            results = self.SQL.mysql_execute(query)

            ## Stop the stopwatch / counter                      
            process_stop_p = time.time()                     
            ## Sum the time of the module
            self.polling_process += process_stop_p - process_start_p

            if results != []:
                
                ## process time
                process_start = time.time() 

                ## Gets the new insertion on Input_UNIZAR table
                for i in range(len(results)):    

                    ## list of files generated
                    names_list = []
                    ## Get data
                    idinput = str(results[i][3])
                    ## The first model of the loop has also the provenance_initial_input column on DB
                    query = f'SELECT {self.SQL.listtostring(self.tyclass.columns, comma=True)}, names_list, path, id_simulation FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(idinput)};'
                    inputs = self.SQL.mysql_execute(query)
                    path = inputs[0][-2]
                    idsim = str(inputs[0][-1])
                    datatoseri = inputs[0][0:len(self.tyclass.columns)]
                    query = f'SELECT `loop` FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim};'
                    loop = self.SQL.mysql_execute(query)[0][0]

                    ## select serializer method
                    if self.type != "A1":
                        names_list = self.tyclass.serializer(path, datatoseri)                   
                    if self.type == "A1":
                        names_list = self.tyclass.serializer(path, idinput, datatoseri, loop)
                        
                    ## Generates checksum
                    checksums = self.calculateChecksum(path, names_list)

                    ## Verifies if the inputs is not repeated
                    if self.type == "T1" and loop == 1:
                        checksums = self.calculateChecksum(path, names_list)
                        self.checkRepetition(path, checksums)
                     
                    ## Update database
                    query = f"UPDATE {self.SQL.mydb.database}.{self.poll_table} SET names_list = \"{self.SQL.listtostring(names_list,True)}\", checksum = \"{self.SQL.listtostring(checksums, True)}\" WHERE id{self.poll_table} = {str(idinput)};"
                    self.SQL.mysql_execute_NOreturn(query)

                    ## Excludes from exec_table the input selected
                    query = f"DELETE FROM {self.SQL.mydb.database}.exec_table WHERE provenance_id = {str(idinput)} AND `table_name` = \"{self.poll_table}\" AND action = \"insert\";"
                    self.SQL.mysql_execute_NOreturn(query)    

                ## Stop the stopwatch / counter 
                process_stop = time.time()   
                ## Sum the time of the module
                self.total_process += process_stop - process_start 

            else:
                time.sleep(self.poll_time)
            
        self.SQL.closeConnection()
        
    
    ## Calculates checksums
    def calculateChecksum(self, path, inputfiles):
        
        checksums = []
        for f in inputfiles:
            if f != "":
                ## Generates checksum
                checksum = subprocess.check_output("md5sum "+path+f, shell=True)
                checksum = checksum.decode().split()[0]
                checksums.append(checksum)
        
        return checksums
    

    ## Verifies if input is new            
    def checkRepetition(self, path, checksums):

        repeated = False

        ## Query to get the checksums calculated
        query = f"SELECT `checksum` FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_simulation IN (SELECT idsimulation FROM {self.SQL.mydb.database}.simulation WHERE id_model = {self.idmodel} AND status = \"Finished\");"
        olds = self.SQL.mysql_execute(query)

        if olds != []:
            for i in range(len(olds)):
                if olds[i] == checksums:
                    repeated = True
                    break
                else:
                    repeated = False

        ## If repeated, check if the files are identical, copy outputs
        ## Convert list to string
        checksums = self.SQL.listtostring(checksums, True)
        if repeated == True:
            
            ## Get id of input to copy the outputs
            query = f"SELECT id{self.pool_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE checksum = {checksums} AND id_simulation IN (SELECT idsimulation FROM {self.SQL.mydb.database}.simulation WHERE id_model = {self.idmodel} AND status = \"Finished\") LIMIT 1);"
            idinput = self.SQL.mysql_execute(query)[0][0]
            query = f"SELECT names_list, path FROM {self.SQL.mydb.database}.output_{self.name} WHERE id_{self.poll_table} = {str(idinput)});"
            result = self.SQL.mysql_execute(query)[0][0]
            filestocopy = result[0][0].split(", ")
            pathtocopy = result[0][1]

            ## check if the content of all files are repeated?
            print("Repeated inputs. Taking solutions previously calculated...")
            ## copy files
            for f in filestocopy:
                cmd = "cp "+pathtocopy+f+" "+path
                os.system(cmd)
    

    def liststostring(self, lists):

        string = str(lists[0])
        for i in range(1,len(lists)):
            string = string+str(lists[i])

        return string