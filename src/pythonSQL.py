## This module gather all access database functions and also functions to be use to build queries
from mysql.connector import connect, Error
import sys
import os

## This module includes all functions that access to database
class SQLpython:

    ## Create a connection with the database
    ## Input:
    ##    check : boolean to check if database exist or not
    def __init__(self, cluster, password, debug, dbname=""):

        ## Print variable
        self.debug = debug

        ## Set DB variables
        self.pwd = password
        if cluster != "local":
            self.hostname = "149.156.182.123"
            self.username = "unibo"
            self.dbpath = f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/database/'
        else:
            self.hostname = "localhost"
            self.username = "root"
            self.dbpath = "../database/"

        ## Creates a connection to the class
        ## if db already was created
        if dbname != "":
            self.mydb = connect(
            host= self.hostname,
            user= self.username,
            passwd= password,
            database = dbname,
            autocommit= True ## To update changes on database
            )
        else:
            self.mydb = connect(
            host= self.hostname,
            user= self.username,
            passwd= password,
            autocommit= True ## To update changes on database
            )

    
    ## Connect and initialize DB in case of does not exist
    def connectDB(self,dbname):
        
        ## Checks if database exists
        cursor = self.mydb.cursor()
        cursor.execute("SHOW DATABASES")
        result = str(cursor.fetchall())
        cursor.close()
        
        ## If database does not exist, create it
        if result.find(f'"{dbname}"') == -1:
            print(f"Database {dbname} does not exists.")
            print("Creating...")
            
            try:
                sql = f"CREATE DATABASE IF NOT EXISTS `{dbname}` DEFAULT CHARACTER SET utf8 ;"
                cursor = self.mydb.cursor()
                cursor.execute(sql)
                cursor.close()
                self.mydb.commit()
                print(f"Database {dbname} was created.")
            except Error as err:
                print("Database was not initialized.") 
                print(err)
                sys.exit(1) 
        else:
            ## Clear exec_table
            cursor.execute(f"DELETE FROM {self.mydb.database}.exec_table;")

        print(f"Connected to {dbname}.")
    
        ## Re-stablish the connection to the new DB
        ## Creates a connection to the class
        self.mydb.close()
        self.mydb = connect(
        host= self.hostname,
        user= self.username,
        passwd= self.pwd,
        database = dbname,
        autocommit= True ## To update changes on database
        )
        
        self.initializeDB()
            
        #### Insert models (Always do it because new models can be inserted.) 
        self.insertModels()
        
        
    ## Initialize DB
    def initializeDB(self):        
        
        ## Create tables
        tables = ["exec", "modeltype", "model", "job"]
        for t in tables:
            with open(f'{self.dbpath}{t}.sql', 'r') as file:
                sql = file.read().replace('<db_name>', self.mydb.database)
            self.mysql_execute_NOreturn(sql) 
            
            
    ## Insert model types and models
    def insertModels(self):
        
        ## Register model' types
        file = open(f'{self.dbpath}insert_modeltypes.sql', 'r')
        sql = file.readline().replace('<db_name>', self.mydb.database)
        while sql != '' and sql != "\n":
            self.mysql_execute_NOreturn(sql) 
            sql = file.readline().replace('<db_name>', self.mydb.database)       
        print(f"Models types were registred on database.") 
        
        ## Insert models      
        file = open(f'{self.dbpath}insert_models.sql', 'r')
        sql = file.readline().replace('<db_name>', self.mydb.database)
        while sql != '' and sql != "\n":
            self.mysql_execute_NOreturn(sql) 
            sql = file.readline().replace('<db_name>', self.mydb.database)       
        print(f"Models were inserted on database.") 
        
    
    ## Create tables
    def createTablesDB(self, models, names):
        
        ## Create initial input table
        with open(f'{self.dbpath}initialinput.sql', 'r') as file:
            sql = file.read().replace('<db_name>', self.mydb.database)
            sql = sql.replace('<first>', str(models[0]))
        self.mysql_execute_NOreturn(sql)  
        
        ## Create final output table
        with open(f'{self.dbpath}finaloutput.sql', 'r') as file:
            sql = file.read().replace('<db_name>', self.mydb.database)
        self.mysql_execute_NOreturn(sql)  
        
        ## Create orchestration table
        with open(f'{self.dbpath}orchestration.sql', 'r') as file:
            sql = file.read().replace('<db_name>', self.mydb.database)
        self.mysql_execute_NOreturn(sql)         
        
        ## Create simulation table
        with open(f'{self.dbpath}simulation.sql', 'r') as file:
            sql = file.read().replace('<db_name>', self.mydb.database)
        self.mysql_execute_NOreturn(sql) 
        
        ## Create provenance_sim table
        with open(f'{self.dbpath}provenance_sim.sql', 'r') as file:
            sql = file.read().replace('<db_name>', self.mydb.database)
        self.mysql_execute_NOreturn(sql)         
        
        ## Initialize input tables
        for n in names:
            with open(f'{self.dbpath}input.sql', 'r') as file:
                sql = file.read().replace('<db_name>', self.mydb.database)
                sql = sql.replace('<model_name>', n)  
            self.mysql_execute_NOreturn(sql)  
            
        ## Initialize output tables
        for n in names:
            with open(f'{self.dbpath}output.sql', 'r') as file:
                sql = file.read().replace('<db_name>', self.mydb.database)
                sql = sql.replace('<model_name>', n)  
            self.mysql_execute_NOreturn(sql)  
            
        ## Create assotiacion with the initial input
        self.bondii(names[0])

        ## add input data columns for input table (specific for each table)
        for i in range(len(models)):
            query = f"SELECT `type` FROM {self.mydb.database}.model WHERE idmodel = {models[i]}"
            tipo = self.mysql_execute(query)[0][0]
            with open(f'{self.dbpath}{tipo}.txt', 'r') as file:
                columns = file.readline()
                while columns != "":
                    self.addcolumn(f"input_{names[i]}", columns)
                    columns = file.readline()
        
        
    ## Create tables
    def insertTriggers(self, models, names):
        
        ## Create initial input trigger
        self.mysql_execute_NOreturn("DROP TRIGGER IF EXISTS bondInitialInput")
        sql = """ CREATE TRIGGER bondInitialInput AFTER INSERT ON input_<first>
                FOR EACH ROW
                BEGIN
                        IF (NEW.`provenance_initial_input` IS NOT NULL) THEN
                                UPDATE initial_input
                                SET `id_input_<first>` = NEW.idinput_<first> WHERE `idinitial_input` = NEW.`provenance_initial_input`;
                        END IF;
                END"""
        sql = sql.replace('<first>', str(names[0]))
        self.mysql_execute_NOreturn(sql)  
        
        ## Create orchestration trigger
        self.mysql_execute_NOreturn("DROP TRIGGER IF EXISTS updateOrchestration")
        sql = """ CREATE TRIGGER updateOrchestration AFTER INSERT ON final_output
                FOR EACH ROW
                BEGIN
                    DECLARE inputdatetime DATETIME;
                    SET inputdatetime = (SELECT `datetime` FROM initial_input WHERE `idinitial_input` = NEW.`id_initial_input`);
                    UPDATE orchestration
                    SET `runtime` = TIMEDIFF(NEW.`datetime`, inputdatetime) WHERE `id_initial_input` = NEW.`id_initial_input`;
                    UPDATE orchestration
                    SET `id_final_output` = NEW.`idfinal_output` WHERE `id_initial_input` = NEW.`id_initial_input`;
                END"""
        self.mysql_execute_NOreturn(sql)               
        
        ## Create input tables triggers
        for n in names:
            self.mysql_execute_NOreturn(f"DROP TRIGGER IF EXISTS {self.mydb.database}.input_{n}_AFTER_INSERT")
            sql = """ CREATE DEFINER = CURRENT_USER TRIGGER `<db_name>`.`input_<model_name>_AFTER_INSERT` AFTER INSERT ON `input_<model_name>` FOR EACH ROW
                    BEGIN
                    INSERT INTO exec_table 
                    VALUES(NULL, "input_<model_name>", NOW(), new.idinput_<model_name>,"insert");
                    END"""
            sql = sql.replace('<db_name>', str(self.mydb.database))
            sql = sql.replace('<model_name>', str(n))
            self.mysql_execute_NOreturn(sql)  

            self.mysql_execute_NOreturn(f"DROP TRIGGER IF EXISTS {self.mydb.database}.`input_{n}_AFTER_UPDATE`")
            sql = """ CREATE DEFINER = CURRENT_USER TRIGGER `<db_name>`.`input_<model_name>_AFTER_UPDATE` AFTER UPDATE ON `input_<model_name>` FOR EACH ROW
                    BEGIN
                    IF new.checksum IS NOT NULL THEN
                    INSERT INTO exec_table
                    VALUES(NULL, "input_<model_name>", NOW(), new.idinput_<model_name>,"update");
                    END IF;
                    END"""
            sql = sql.replace('<db_name>', str(self.mydb.database))
            sql = sql.replace('<model_name>', str(n))
            self.mysql_execute_NOreturn(sql)  
            
        ## ICreate output tables trigger
        for n in names:
            with open(f'{self.dbpath}output_triggers.sql', 'r') as file:
                self.mysql_execute_NOreturn(f"DROP TRIGGER IF EXISTS {self.mydb.database}.`output_{n}_AFTER_INSERT`")
                sql = """ CREATE DEFINER = CURRENT_USER TRIGGER `<db_name>`.`output_<model_name>_AFTER_INSERT` AFTER INSERT ON `output_<model_name>` FOR EACH ROW
                        BEGIN
                        INSERT INTO exec_table 
                        VALUES(NULL, "output_<model_name>", NOW(), new.idoutput_<model_name>,"insert");
                        END"""
                sql = sql.replace('<db_name>', str(self.mydb.database))
                sql = sql.replace('<model_name>', str(n))
                self.mysql_execute_NOreturn(sql)  

        print("Triggers created.")
                    
        
    ## Add ONE column if does not exist. Returns if column was added
    def addcolumn(self, table, columnsql):
        
        ## Get column name
        colname = columnsql.split(" ")[0]
        ## Check if column exist
        query = f"SHOW columns FROM {self.mydb.database}.{table}"
        columns = self.mysql_execute(query)
        ## Check if column exist
        exist = False
        for c in columns:
            if c[0] == colname:
                exist = True
                break
        ## If does not exist, create it
        if exist == False:
            query = f"ALTER TABLE {self.mydb.database}.{table} ADD COLUMN {columnsql}"
            self.mysql_execute_NOreturn(query) 
            return True
        return False
      
            
    ## Bond initial input to the first input table
    def bondii(self, firstmodelname):
        
        ## Add foreign column on initial_input table
        added = self.addcolumn("`initial_input`", f"id_input_{firstmodelname} INT NULL")
        if added == True:
            query = f"""ALTER TABLE `{self.mydb.database}`.`initial_input` ADD CONSTRAINT `fk_initial_input_{firstmodelname}`
                        FOREIGN KEY (`id_input_{firstmodelname}`)
                        REFERENCES `{self.mydb.database}`.`input_{firstmodelname}` (`idinput_{firstmodelname}`)
                        ON DELETE CASCADE
                        ON UPDATE NO ACTION"""
            self.mysql_execute_NOreturn(query) 

        ## add foreign column on input table of the first model
        added = self.addcolumn(f"`input_{firstmodelname}`", "provenance_initial_input INT NULL")
        ## Add constraint 
        if added == True:
            query = f"ALTER TABLE `{self.mydb.database}`.`input_{firstmodelname}` ADD CONSTRAINT `fk_initial_input` FOREIGN KEY (`provenance_initial_input`) REFERENCES `{self.mydb.database}`.`initial_input` (`idinitial_input`) ON DELETE CASCADE ON UPDATE NO ACTION"
            self.mysql_execute_NOreturn(query) 
        

    ## Execute a SQL query with return 
    ## Input: string of the SQL query
    def mysql_execute(self, querysql):

        cursor = self.mydb.cursor()
        self.mydb.ping(reconnect=True, attempts=1000, delay=1) 
        try:
            cursor.execute(querysql)
        except:
            print("Query "+querysql+" failed.")
            sys.exit()
        result = cursor.fetchall()
        cursor.close()
        return result

    ## Execute SQL query without return (e.g. insertion queries)
    ## Input: string of the SQL query
    def mysql_execute_NOreturn(self, querysql):

        cursor = self.mydb.cursor()
        self.mydb.ping(reconnect=True, attempts=1000, delay=1) 
        try:
            cursor.execute(querysql)
            self.mydb.commit()
            cursor.close()
        except Error as err:
            print("Something went wrong: {}".format(err))
            print("Query "+querysql+" failed.")
            sys.exit(1)


    ## Execute SQL query without return (e.g. insertion queries)
    ## Input: string of the SQL query
    def mysql_executemany_NOreturn(self, queriessql):

        cursor = self.mydb.cursor()
        self.mydb.ping(reconnect=True, attempts=1000, delay=1) 
        try:
            for query in queriessql:
                cursor.execute(query)
            self.mydb.commit()
            cursor.close()
        except Error as err:
            print("Something went wrong: {}".format(err))
            print("Multiple queries commition failed.")
            sys.exit(1)


    ## Insert data
    ## Inputs: Table and a list with values without ID.  
    ## The function considers that the ID will be auto generated
    def mysql_insert(self, table, columns, data):

        cursor = self.mydb.cursor()
        self.mydb.ping(reconnect=True, attempts=1000, delay=1) 
        columns = "INSERT INTO "+self.mydb.database+"."+table+"("+columns+") "
        values = 'VALUES ('+data+');'
        query = columns+values
        cursor.execute(query)
        self.mydb.commit()
        cursor.close()
        if self.debug == True:
            print("A new line was inserted on table "+table+".")

    ## Delete data
    ## Inputs: Table name, a column name and the value to be excluded.
    def mysql_delete(self, table, column_name, value):

        cursor = self.mydb.cursor()
        self.mydb.ping(reconnect=True, attempts=1000, delay=1)  
        query = "DELETE FROM "+self.dbname+"."+table+ " WHERE " +column_name + " = " + str(value)
        cursor.execute(query)
        self.mydb.commit()
        cursor.close()
        if self.debug == True:
            print("A line was removed on table "+table+"!")


    ## Close connection
    def closeConnection(self):
        self.mydb.close()


    ## Converts list to string
    def listtostring(self, l, comma = False, space = False):
        text = ''
        if l != []:
            for i in range(len(l)-1):
                if l[i] != '':
                    if comma == True:
                        line = str(l[i])+','
                    else:
                        if space == True:
                            line = str(l[i])+' '
                        else:
                            line = str(l[i])
                    text = text+line  
            text = text+str(l[-1])
            text.encode('utf-8')
        return text


    ## Converts list to string
    def liststostring(self, l, space = False):
        text = ''
        if l != []:
            for j in range(len(l[0])):
                for i in range(len(l)-1):
                    if l[i][j] != '':
                        if space == True:
                            line = str(l[i][j])+' '
                        else:
                            line = str(l[i][j])
                        text = text+line  
                text = text+str(l[i+1][-1])+"\n"
            text.encode('utf-8')
        return text
    



