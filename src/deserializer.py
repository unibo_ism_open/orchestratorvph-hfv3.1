import os
import sys
import time
import O1 as O1
import A1 as A1
import C1 as C1
import M1 as M1
import subprocess
import pythonSQL as pysql

class deserializer:

    def __init__(self, dbname, name, idmodel, cluster, iofolder, modelspath, pwd, poll_time, nGPUs):

        ## Model name (for prints)
        self.name = name
        
        ## Cluster name
        self.cluster = cluster
        
        ## Creates a instance of SQLpython class with a new database connection 
        self.SQL = pysql.SQLpython(cluster, pwd, False, dbname)

        ## Save name and ID of the model
        m = f"SELECT exec_name, type FROM {self.SQL.mydb.database}.model WHERE idmodel ={str(idmodel)}; "
        m = self.SQL.mysql_execute(m)
        if m != []:
            self.model = m[0][0]
            self.idmodel = idmodel
            self.type = m[0][1]
        else:
            sys.exit("There is no model with ID "+str(self.idmodel)+" inserted.")

        ## Stopping criterion 
        self.finished = False

        ## Set number of GPUs
        if self.type == "A1":
            self.nGPUs = nGPUs

        ## Time variables
        self.total_process = 0
        self.polling_process = 0

        ## Set the interval in seconds to pool
        self.poll_time = poll_time

        ## Set the name of I/O folder
        self.iofolder = iofolder

        ## table name to poll
        self.poll_table = 'input_'+name
        ## table name to write inputs
        self.write_table = 'output_'+name
            
        ## Set SCRATCH        
        if self.cluster == "ares":
            self.scratch =  os.environ["SCRATCH"]## temporary file system (faster)
        else:
            if self.cluster == "local":
                self.scratch = "../../scratch/people/plgvarella/" ## temporary file system (faster) 

        ## Save structs based on provenance id
        self.jobs = [] ## [prov id simulation, job]
        self.provdata= [] ## [prov id simulation, nelems,nbins*total, inputsperjob, current(counter), goal]   
        
        ## Create instance of model type class
        if self.type == "O1":
            self.tyclass = O1.O1(self.SQL, name, cluster, modelspath)
        if self.type == "C1":
            self.tyclass = C1.C1(self.SQL, name, cluster, modelspath)
        if self.type == "A1":
            self.tyclass = A1.A1(self.SQL, name, cluster, modelspath)
        if self.type == "M1":
            self.tyclass = M1.M1(self.SQL, name, cluster, modelspath)

        ## Verify if there are inputs interrupteds
        query = f"""SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} NOT IN 
        (SELECT id_{self.poll_table} FROM {self.SQL.mydb.database}.{self.write_table}) AND checksum IS NOT NULL;"""
        ## Get unfinished IDs. They were interrupted or are waiting a model to finish
        unfinish = self.SQL.mysql_execute(query)
        ## Get IDs running
        query = f'SELECT idinputs FROM {self.SQL.mydb.database}.job WHERE description = "{self.type} model" AND (status = \"running\" OR status = \"pending\");'
        runs = self.SQL.mysql_execute(query)
        ## Build simple lists
        unfinisheds = []
        for u in unfinish:
            unfinisheds.append(int(u[0]))
        runnings = []
        for r in runs:
            ids = r[0].split()
            for id in ids:
                runnings.append(int(id))

        ## Get interrupteds
        interrupteds = list(set(unfinisheds) - set(runnings))
        ## For each interrupted, write on exec_table
        for interrupted in interrupteds:
            query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(interrupted)};"
            id = self.SQL.mysql_execute(query)[0][0]
            columns = f"`table_name`, `datetime`, `provenance_id`, `action`"
            values = f"\"{self.poll_table}\", NOW(), {id}, 'update'"
            self.SQL.mysql_insert("exec_table", columns, values)


    def wait(self):

        while self.finished == False:

            ## process time
            process_start_p = time.time() 
            
            ## Pools the exec table
            results = self.SQL.mysql_execute(f"SELECT * FROM {self.SQL.mydb.database}.exec_table WHERE `table_name` = \"{self.poll_table}\" AND action = \"update\";")

            ## Stop the stopwatch / counter                  
            process_stop_p = time.time()                     
            ## Sum the time of the module
            self.polling_process += process_stop_p - process_start_p


            if results != []:
                
                ## process time
                process_start = time.time()

                ok = False ## Check if job was called or if the calling failed 

                ids = ""
                for i in range(len(results)-1):
                    ids = ids+str(results[i][3])+","
                ids = ids+str(results[-1][3])

                ## Check if input exist, otherwise register empty output and finish orchestration
                query = f"SELECT names_list FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} IN ({ids});"
                names_list = self.SQL.mysql_execute(query)
                for i in range(len(names_list)):
                    if n == "": ## failure
                        id = results[i][3]
                        ## Deserialize
                        self.tyclass.deserializer(id, False)
                    
                ## Get provenance_sim
                query = f"SELECT DISTINCT provenance_sim FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} IN ({ids}) ;"
                prov_sims = self.SQL.mysql_execute(query)
                first = False

                ## if first model
                if prov_sims[0][0] is None:
                    ## Get provenance_sim
                    query = f"SELECT DISTINCT provenance_initial_input FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} IN ({ids}) ;"
                    prov_sims = self.SQL.mysql_execute(query)
                    first = True

                for prov in prov_sims:

                    prov_sim = prov[0]

                    if first == True:
                        provcolumn = "provenance_initial_input"
                    else:
                        provcolumn = "provenance_sim"

                    ## Get one example of input with the provenance_sim returned
                    query = f"SELECT path, id_simulation, id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE {provcolumn} = {prov_sim} LIMIT 1;"
                    input = self.SQL.mysql_execute(query)
                    path = input[0][0]
                    idsim = str(input[0][1])
                    idinput = str(input[0][2])

                    ## select de-serializer method
                    if self.type == "A1":
                        [callJob, idinputs]  = self.countInputsReady(prov_sim, path, ids)
                        if callJob == True:
                            self.tyclass.callModel(path, idinputs, self.idmodel, time=2*len(idinputs))
                    else:
                        self.tyclass.callModel(path, idinput, self.idmodel, idsim) 
                        idinputs = [idinput]                       
                        
                    time.sleep(15) ## Give time to then check if job was started  
                    
                    ok = self.checkCall(path) 
                    print(f'ok ={ok}')
                    
                    ## Excludes ok calls from exec_table the input selected
                    ## Get all provenance simulations
                    if ok == True:
                        idscalled = ""
                        for id in (idinputs):
                            idscalled = idscalled+str(id)+","
                        idscalled = idscalled[:-1]
                        if idscalled != "":
                            query = f"DELETE FROM {self.SQL.mydb.database}.exec_table WHERE provenance_id IN ({idscalled}) AND `table_name` = \"{self.poll_table}\" AND action = \"update\";"
                            self.SQL.mysql_execute_NOreturn(query) 
                            
                ## Stop the stopwatch / counter                     
                process_stop = time.time()                    
                ## Sum the time of the module
                self.total_process += process_stop - process_start      
            
            else:
                time.sleep(self.poll_time) 
                
                
            ## process time
            process_start = time.time()                       
                    
            self.checkStart()                  
            self.checkEnd()  
            
            ## Stop the stopwatch / counter                     
            process_stop = time.time()                    
            ## Sum the time of the module
            self.total_process += process_stop - process_start                
        

        self.SQL.closeConnection()

    
    ## Verify if there are jobs called to this provenance simulation and if there are inputs enough to create a new job
    def countInputsReady(self, prov_sim, path, ids):

        ## Initialize output vars
        callJob = False
        listids = []

        ## Get inputs per job
        query = f"SELECT inputs_per_job FROM {self.SQL.mydb.database}.provenance_sim WHERE idprov = {prov_sim};"
        iperjob = self.SQL.mysql_execute(query)[0][0]

        ## get amount of jobs called
        query = f"SELECT count(id) FROM {self.SQL.mydb.database}.job WHERE lower(errorfile) LIKE \"{path}%\""
        njobscalled = self.SQL.mysql_execute(query)[0][0]

        ## Calculate number of ids of the next job
        ncalled = round(njobscalled*iperjob)
        nnext = round((njobscalled+1)*iperjob)
        ninputs = nnext - ncalled

        ## Select jobs ready from this provenance_sin
        query = f"SELECT idinput_{self.name} FROM {self.SQL.mydb.database}.input_{self.name} WHERE provenance_sim = {prov_sim} AND idinput_{self.name} IN ({ids});"
        idslist = self.SQL.mysql_execute(query)
        print(f'Jobs called: {njobscalled}; next: {ninputs}; current:{len(idslist)}')
         ## Check if new job can be called
        if len(idslist) >= ninputs: ## can call job
            callJob = True
            ## Tranpose 1D matrix to list
            for i in range(ninputs):
                listids.append(idslist[i][0])

        return [callJob, listids]


    ## Check if Rimrock command did not failed
    def checkCall(self, path):

        ## if find the path on log file, job was called with success
        ok = False
        if self.cluster == "local":
            self.outputfile = open("../output.out",'r')## The master logfile must to have this name
        else:
            self.outputfile = open(f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/slurm-{os.environ["SLURM_JOB_ID"]}.out','r')
        lines = self.outputfile.readlines()
        for i in range(len(lines)-1,-1,-1):
            found = lines[i].find(path)
            if found > -1:
                ok = True
                break
        self.outputfile.close()         
        
        return ok       
        

    ## Verifies if job was called with success
    def checkStart(self):
        
        ## Get jobs that are pending
        query = f"SELECT id, logfile FROM {self.SQL.mydb.database}.job WHERE id_model = \"{self.idmodel}\" AND idjob IS NULL"
        result = self.SQL.mysql_execute(query) 
        
        for r in result:
            id = r[0]
            logfile = r[1]
            
            ## Check logfile to verify if it was called
            ## The first print in the log is the jobID
            if os.path.isfile(logfile) == True:
                log = open(logfile, 'r')
                ## read first line
                line = log.readline()
                
                if line != '':
                    ## update job table
                    query = f"UPDATE {self.SQL.mydb.database}.job SET idjob = {line}, status = \"running\" WHERE id = {id}"
                    try:
                        self.SQL.mysql_execute_NOreturn(query)
                    except:## Wait log file be written
                        time.sleep(1)  

        
    ## Verify if the job ended
    ## The goal of this method id to identify if a job has finished with success or not. In case of error, a function of the class model will identify the error.
    def checkEnd(self):
        
        ## Get jobs that are running
        query = f"SELECT id, idjob, logfile, errorfile FROM {self.SQL.mydb.database}.job WHERE id_model = \"{self.idmodel}\" AND status = \"running\""
        result = self.SQL.mysql_execute(query) 
        
        for r in result:
            id = r[0]
            idjob = r[1]
            logpath = r[2]
            errorpath = r[3]

            query = f"SELECT idinputs, modelfile FROM {self.SQL.mydb.database}.job WHERE id = {id}"
            results = self.SQL.mysql_execute(query)
            idslist = results[0][0].split()
            modelfile = results[0][1]
            
            path = errorpath.split("error")[0]
            
            ## Check logfile if execution finished
            log = open(logpath, 'r')
            lines = log.readlines()
            line = ""
            if len(lines) > 1: ## If file is not empty
                line = lines[-1]
            efile = open(errorpath,'r')
            elines = efile.readlines()
            status = ""
            if line.startswith("Done.") == True:## If finished
                status, idsfinished = self.tyclass.checkStatus(self.cluster, id, idjob, errorpath)
            else: ## If failed
                ## check error file (if job interrupted)
                for line in elines:
                    ## convert any interruption as ERROR
                    if line.find("Aborted") >= 0 or line.find("error") >=  0 or line.find("DUE TO TIME LIMIT") >= 0 or line.find("out-of-memory") >= 0 or line.find("CANCELLED") >= 0:
                        status, idsfinished = self.tyclass.checkStatus(self.cluster, id, idjob, errorpath)
            if status == "COMPLETED":
                ## Deserialize
                self.tyclass.deserializer(idsfinished, True)
                ## Delete model copy
                cmd = f"rm {path}{modelfile}"
                os.system(cmd)
                ## Get execution time
                timeexec = self.getTime(idjob)
                ## Update job status
                query = (f'UPDATE {self.SQL.mydb.database}.job SET status = \"Finished\", dt_end = NOW(), execution_time = "{timeexec}" WHERE id = {id};')
                self.SQL.mysql_execute_NOreturn(query)
                ## Calculate queue time
                query = (f'UPDATE {self.SQL.mydb.database}.job SET queue_time = TIMEDIFF(TIMEDIFF(dt_end,dt_creation),execution_time) WHERE id = {id};')
                self.SQL.mysql_execute_NOreturn(query)
                print(f'JOB {idjob} has finished succesfully')
            else:
                if status == "ERROR":
                    print(f"Job {idjob} failed.")
                    ## Update DB with failure case for the last id
                    for i in range(len(idslist)-1):
                        print(f'ID {idslist[i]} finished with success.')
                        ## Deserialize
                        self.tyclass.deserializer(idslist, True)
                    ## Update DB with failure
                    print(f'ID {idslist[-1]} failed.')
                    ## Deserialize
                    self.tyclass.deserializer(idslist, False)
                    ## Update job status
                    query = f"UPDATE {self.SQL.mydb.database}.job SET status = \"Error\", dt_end = NOW() WHERE id = {id};"
                    self.SQL.mysql_execute_NOreturn(query) 
                    print(f'JOB {idjob} has finished with problem.')

                else:
                    if status == "CANCELLED":
                        ## Update DB with interrupted case
                        for idinput in idslist:
                            ## CHECK FOR EACH ID IF IT FAILED
                            ## Get id simulation
                            query = f"SELECT id_simulation FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(idinput)};"
                            idsim = str(self.SQL.mysql_execute(query)[0][0])
                            print(f'Job of the simulation ID {idsim} failed for error.')
                            ## Deserialize
                            self.tyclass.deserializer(idslist, False)
                            ## Update job status
                            query = f"UPDATE {self.SQL.mydb.database}.job SET status = \"Cancelled\", dt_end = NOW() WHERE id = {id};"
                            self.SQL.mysql_execute_NOreturn(query) 
                            print(f'JOB {idjob} has been interrupted.')
                    else:
                        if status.find("RUNNING") == -1 and status.find("RE-CALLED") == -1  and status != "":
                            print(f'JOB {idjob} has unknown status:{status}')
                        
    
    ## Get job execution time
    def getTime(self, jobid):

        print("Getting time.")
        
        if self.cluster != "local":
            cmd = f'sacct -j {jobid} --format="Elapsed"'
            jobinfo = subprocess.check_output(cmd, shell=True)
            jobinfo = jobinfo.decode()
            jobinfo = jobinfo.split("\n")
            print(jobinfo)
            time = jobinfo[-3]
        
        if self.cluster == "local":
            arq = open("../time.txt",'r')
            jobinfo = arq.readlines()
            line = jobinfo[-1]
            time = line.split()[-4]

        return time
                