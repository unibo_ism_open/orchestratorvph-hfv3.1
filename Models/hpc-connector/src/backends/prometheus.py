from src.Backend import Backend
from src.Job import Job
import json

class Prometheus (Backend):
    
    JOBS_REQUIRED_ARGUMENTS = ['script', 'host']
    JOBS_OPTIONAL_ARGUMENTS = ['working_directory', 'tag']

    __STATE_TO_STR = { "QUEUED": Job.PENDING,  "RUNNING": Job.RUNNING, "FINISHED": Job.COMPLETED, "ABORTED": Job.CANCELLED, "ERROR": Job.FAILED }

    def __init__(self, _id):
        self._id = _id
        return super(Prometheus, self).__init__(_id)

    def configure_backend (self, data):
        result = True
        if 'ENDPOINT' in data:
            self._endpoint = data['ENDPOINT']
        else:
            print('Error: ENDPOINT is not specified in backend configuration')
            result = False
        if 'PROXY' in data:
            self._auth_header = { "PROXY": data['PROXY'] }
        else:
            print( 'Error: PROXY is not specified in backend configuration')
            result = False
        return result
    
    def get_job_config(self, job_config):
        return super(Prometheus, self).get_job_config(job_config, self.JOBS_REQUIRED_ARGUMENTS, self.JOBS_OPTIONAL_ARGUMENTS)
    
    def job_submit(self, job):
        URL = self._endpoint + '/api/jobs'
        METHOD = 'POST'
        HEADERS = self._auth_header
        HEADERS["Content-Type"] = "application/json"        

        response = self._create_request(METHOD, URL, headers=HEADERS, body=json.dumps(job.get_configuration()), verify=False )
        
        if response['status_code'] != 201:
            print("Error launching the job (status_code=%d): %s" %(response['status_code'] ,response['text']) )
            return None
        else:
             # %s" %(response['text']) )
            job.set_info(response['json'])

            if 'job_id'in response['json']:
                job.set_id(response['json']['job_id'])

            if 'status' in response['json']: 
                if response['json']['status'] in self.__STATE_TO_STR:
                    job.set_state(self.__STATE_TO_STR[response['json']['status']])
            
            return True
    
    def job_get_state (self, job):
        URL = self._endpoint + '/api/jobs/' + job.get_id()
        METHOD = 'GET'
        HEADERS = self._auth_header
        response = self._create_request(METHOD, URL, headers=HEADERS, verify=False)
        if response['status_code'] != 200:
            print("Error getting information of the job %s (status_code=%d): %s" %(job.get_id(), response['status_code'] ,response['text']) )
        else:
            job.set_info(response['json'])
            if 'status' in job.get_info()['data']: 
                if job.get_info()['data']['status'] in self.__STATE_TO_STR:
                    job.set_state(self.__STATE_TO_STR[ job.get_info()['data']['status'] ])         
            return True
        return False

    def job_delete (self, job):
        URL = self._endpoint + '/api/jobs/' + job.get_id()
        METHOD = 'DELETE'
        HEADERS = self._auth_header

        response = self._create_request(METHOD, URL, headers=HEADERS, verify=False, store_json=False)
        '''
        if response['status_code'] != 204:
            print("Error deleting job %s (status_code=%d): %s" %(job.get_id(), response['status_code'] ,response['text']) )
            job.set_info(response['json'])
            
            return False
        '''    
        return True

    def job_cancel (self, job):
        URL = self._endpoint + '/api/jobs/' + job.get_id()
        METHOD = 'PUT'
        HEADERS = self._auth_header
        HEADERS["Content-Type"] = "application/json"        
        response = self._create_request(METHOD, URL, headers=HEADERS, verify=False, body=json.dumps({"action":"abort"}), store_json=False)
        
        if response['status_code'] != 204:
            print("Error aborting the job %s (status_code=%d): %s" %(job.get_id(), response['status_code'] ,response['text']) )
            job.set_info(response['json'])
            
            return False
            
        return True

    def file_download (self, src):
        file_content = None
        HEADERS = self._auth_header
        METHOD = 'GET'
        URL = src

        response = self._create_request(METHOD, URL, headers=HEADERS, body=None, verify=False, store_json=False)
        if response['status_code'] == 200:
            file_content = response['text']

        return file_content

    def job_get_logs(self, job, logs_config):
        msg = ""
        for file_path in ['stdout_path']: #, 'stderr_path']:
            if file_path in job._info['data']:
                content = self.file_download(job._info['data'][file_path])
                if content:
                    msg += 'Content of file: "%s"\n%s' % (job._info['data'][file_path], content)
                else: 
                    print('Cannot download the file "%s"'%(job._info['data'][file_path]))
        return msg

    def help(self):
        msg = "--backend-conf dictionary: \n"
        msg += "\t- ENDPOINT: Complete host url. Example: https://submit.plgrid.pl \n"  
        msg += "\t- PROXY: Value of PROXY header \n"   
        msg += "\nJOB --job-config dictionary: \n"  
        msg += "\t- script: String with the SLURM script \n"  
        msg += "\t- host: String with host. Example: prometheus.cyfronet.pl \n"
        msg += "\t- working_directory (optional): By default working_directory is set to user home directory\n"
        msg += "\t- tag (optional)\n"
        msg += "\nSIMULATE --logs-config dictionary: Not required, logs file path are obtained in the job information"  
        print(msg)
