# -*- coding: utf-8 -*-
import src.Job
import requests
import time
import json

requests.packages.urllib3.disable_warnings() 

class Backend(object):
    
    _max_retries = 10
    _sleep_time = 10

    JOBS_REQUIRED_ARGUMENTS = []
    JOBS_OPTIONAL_ARGUMENTS = []

    def __init__(self, _name):
        self._name = _name
    
    def configure_backend (self, data):
        print('Dummy configure_backend - implement it!')
        return False
    
    def get_job_config(self, job_config, JOBS_REQUIRED_ARGUMENTS, JOBS_OPTIONAL_ARGUMENTS):
        new_config = {}
        error = False

        # Required arguments
        for arg in self.JOBS_REQUIRED_ARGUMENTS:
            if not arg in job_config:
                print('Error: "%s" is not specified in job configuration' %(arg))
                error = True
            else:
                new_config[arg] = job_config[arg]

        # Optional arguments
        for arg in self.JOBS_OPTIONAL_ARGUMENTS:
            if arg in job_config:
                new_config[arg] = job_config[arg]

        if error:
            new_config = {}
        
        return new_config

    def help(self):
        print( 'Help not implemented for backend %s' % (self._name))
        
    def get_name(self):
        return self._name

    def job_submit(self, job):
        print('Job submit not implemented for backend %s' % (self._name))
        return None
        
    def job_stop(self, job):
        print('Job stop not implemented for backend %s' % (self._name))
        return None

    def job_delete (self, job):
        print('Job delete not implemented for backend %s' % (self._name))
        return None

    def job_cancel (self, job):
        print('Job cancel not implemented for backend %s' % (self._name))
        return None

    def job_get_state (self, job):
        print('Job info not implemented for backend %s' % (self._name))
        return job.get_state()

    def job_is_running (self, job):
        if job_get_state(job) == Job.RUNNING:
            return True
        return False

    def job_get_logs (self, job, config=None):
        print('Job get logs not implemented for backend %s' % (self._name))
        return None

    def file_upload (self, src, dest):
        print('File upload not implemented for backend %s' % (self._name))
        return False 

    def file_download (self, src):
        print('File download not implemented for backend %s' % (self._name))
        return None

    def file_remove (self, src):
        print('File remove not implemented for backend %s' % (self._name))
        return False  

    def directory_list (self, path):
        print('Directory list not implemented for backend %s' % (self._name))
        return False  

    def directory_create (self, path):
        print('Directory create not implemented for backend %s' % (self._name))
        return False  

    def directory_remove (self, path):
        print('Directory remove not implemented for backend %s' % ((self._name)))
        return False  

    def _create_request(self, method, url, headers=None, body=None, auth_data=None, verify=True, store_json=True):    
        if body is None: 
            body = {}
        if headers is None:
            headers = {}
        
        auth = None
        if auth_data is not None:
            if 'user' in auth_data and 'passwd' in auth_data:
                auth=requests.auth.HTTPBasicAuth( auth_data['user'], auth_data['passwd'])

        response = {}
        retries = 0
        ok = False
        while (self._max_retries > retries) and (not ok) :
            retries += 1
            try: 
                r =  requests.request(method, url, verify=verify, headers=headers, data=body, auth=auth)
                response[ 'status_code' ] = r.status_code
                response[ 'text' ] = r.text
                response[ 'json' ] = {}
                if store_json:
                    response[ 'json' ] = r.json()
                ok=True
            except requests.exceptions.ConnectionError:
                print("Cannot connect to %s, waiting %d seconds..." % (url, self._sleep_time))
                time.sleep(self._sleep_time)
            except:
                print("Not JSON response")

        if not ok:
            print("Cannot connect to %s . Retries: %s" % (url, retries))
            response[ 'status_code' ] = -1
            response[ 'text' ] = 'No response text'
            response[ 'json' ] = {} 

        return response