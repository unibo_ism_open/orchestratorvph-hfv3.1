#!/usr/bin/python

# 
# Copyright (C) 2011 - GRyCAP - Universitat Politecnica de Valencia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from src import __version__ as version
from src import __author__ as author


setup(name='hpc-connector',
    version=version,
    description='HPC - connector',
    author=author,
    author_email='serlohu@upv.es',
    url='https://gitlab.com/primageproject/hpc-connector',
    packages = [ 'src', 'src/backends' ],
    data_files = [ ],
    scripts=['hpc-connector.py'],
    install_requires = ["requests"]
)
