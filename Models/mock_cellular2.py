import sys


class Model:

    def __init__(self, id):
        
        self.input = "vascularization.txt"
        self.file1 = "drug_effects.csv"
        self.file2 = "start_effects.csv"
        self.file3 = "end_effects.csv"
        self.output1 = f"log_{id}.out"
        self.output2 = f"error_{id}.err"
        self.output3 = "jobinfo.txt"
        self.path = '../../../../'


    def main(self):

        ## Create output files
        file1 = open(self.file1, 'w')
        file2 = open(self.file2, 'w')
        file3 = open(self.file3, 'w')
        out1 = open(self.output1, 'w')
        out2 = open(self.output2, 'w')
        out3 = open(self.output3, 'w')
        
        ## get number of lines
        ifile = open(self.input, 'r')
        content = ifile.readlines()
        nelems = len(content)

        ## Write files
        file1.write("CHEK1,JAB1,HIF1A,MYCN,TEP1,p53\n") 
        for i in range(nelems):
            file1.write("1.0,1.0,0.0,1.0,1.0,0.0\n")  
        file2.write("start_effects\n0\n215")
        file3.write("end_effects\n144\n334")
        out1.write("23456\nDone.")
        out2.write(""" plgrid/libs/libpng/1.2.52 loaded.
            plgrid/tools/gcc/6.4.0 loaded.
            plgrid/tools/intel/18.0.0 loaded.
            plgrid/tools/impi/2018 loaded.
            plgrid/apps/ansys/2019R3 loaded.
            srun: Job step aborted: Waiting up to 182 seconds for job step to finish.""")
        #out2.write("""slurmstepd: error: *** JOB 23406045 ON p0597 CANCELLED AT 2022-05-27T17:25:08 DUE TO TIME LIMIT ***
            #slurmstepd: error: *** STEP 23406045.0 ON p0597 CANCELLED AT 2022-05-27T17:25:08 DUE TO TIME LIMIT ***""")
        out3.write("""     State 
            ---------- 
            COMPLETED
            COMPLETED 
            """)

        file1.close()
        file2.close()
        file3.close()

    ## Converts list to string
    def printListOnFile(self, list, arq):

        for i in range(len(list)):
            arq.write(list[i])



if __name__ == "__main__":

    m = Model(sys.argv[1])
    m.main()
    

