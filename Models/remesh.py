# Version: 5.1 2021-10-03
# Uploading the mesh
import pymeshlab
import math
import os
import sys
import vtk
import numpy as np
from shutil import copyfile

#firstly, save old mesh to interpolate later
copyfile('mesh_nodes.inp', 'mesh_nodes_prev.inp')
copyfile('mesh_elements.inp', 'mesh_elements_prev.inp')

#transform variables due to convection (x*Vi/Vf)
# get volume change ratio
vol_i = np.genfromtxt(r'volume_i.txt')
vol_f = np.genfromtxt(r'volume_f.txt')
vol_inc = vol_i / vol_f 
# get variables and update them
# o2_i = np.genfromtxt(r'updated_O2.txt') # removed from schema 03/2022
# updated_cellularity has 6 columns: 
# [1]: alive NB vol fraction, [2]: alive SC vol fraction
# [3]: apoptotic NB vol fraction, [4]: necrotic NB vol fraction, [5]: apoptotic SC vol fraction, [6]: necrotic SC vol fraction
cellularity_i = np.genfromtxt(r'updated_cellularity.txt')

# o2_f = o2_i * vol_inc # removed from schema 03/2022
#cellularity_f = cellularity_i * vol_inc
cellularity_f = cellularity_i # we consider that the volume fractions do not change 

# save variables
# np.savetxt(r'updated_O2.txt',o2_f) # removed from schema 03/2022
np.savetxt(r'updated_cellularity.txt',cellularity_f)

ms = pymeshlab.MeshSet()
ms.load_new_mesh( "geometry.stl") #Load the ply mesh to meshlab
ms.apply_filter('compute_normal_for_point_clouds') #Apply filter to compute the normals from the given set of points
ms.apply_filter('generate_surface_reconstruction_screened_poisson', depth=7) #Apply filter to reconstruct the surface based on those normals and the set of points
#change depth variable to get smoother (lower value for depth) or sharper (greater value for depth) geometries
ms.apply_filter('meshing_remove_duplicate_vertices')
ms.apply_filter('apply_coord_hc_laplacian_smoothing') #Smooth the surface obtained
# ms.save_current_mesh('step1.stl') # Save the obatined surface as an STL file
ms.apply_filter('meshing_decimation_quadric_edge_collapse', targetfacenum = 5000, planarquadric=True,
                autoclean = True, qualitythr=0.9, planarweight=0.1, boundaryweight = 0.5) #Apply filter to decimate the stl geometry
# # https://pymeshlab.readthedocs.io/en/0.1.8/filter_list.html
# #Targetfacenum is the number of faces we want the final geometry to have. Change this number if ANSYS meshing fails
# #Planarquadric keeps triangles in the flat surfaces so the external mesh is not distorted. This must be always True
# #planarweight is related to the previous feature. 0.1 seems to be the sweet spot for the cases avalible currently
ms.apply_filter('meshing_remove_duplicate_vertices')
ms.apply_filter('apply_coord_hc_laplacian_smoothing') # Smooth the simplified surface
ms.save_current_mesh("mesh_new.stl") # Save the obatined surface as an STL file

reader = vtk.vtkSTLReader()
reader.SetFileName("mesh_new.stl")
reader.Update()
polydata = reader.GetOutput()
points = polydata.GetPoints()
nb_point = points.GetNumberOfPoints()
coords = np.array([points.GetPoint(i) for i in range(nb_point)])

nb_cell = polydata.GetNumberOfCells()
connect = np.zeros((nb_cell, 3), dtype=int)

for icell in range(nb_cell):
    ids = polydata.GetCell(icell).GetPointIds()
    connect[icell, 0] = ids.GetId(0) + 1
    connect[icell, 1] = ids.GetId(1) + 1
    connect[icell, 2] = ids.GetId(2) + 1

node = coords
element = connect  # need to check order if contact exist
node1 = np.append(np.arange(1,len(coords)+1).reshape(-1,1),coords,axis=1)
element1 = np.append(np.arange(1,len(connect)+1).reshape(-1,1),connect,axis=1)
# In[4]:
#Write the nodes and triangles from stl file in an .inp file
#The last commands create a 3D volume mesh from stl mesh and save it in a cdb file    

with open('tumour_mesh.inp','w') as fint:
    fint.write("""/units,SI
/prep7
et,2,170
type,2\n""")
    np.savetxt(fint,node1,fmt='n,%i,%.6f,%.6f,%.6f')
    np.savetxt(fint, element1,fmt='en,%i,%i,%i,%i')
    fint.write("""et,1,285
mp,dxx,1,0.001
type,1
mat,1
fvmesh\n""")
    fint.write("""
! ----------------------------------------------------------------------------
! - WRITE MESH
! ----------------------------------------------------------------------------
nsel,all
*Get,num_nodes,NODE,0,COUNT
*GET,node_,NODE,0,NUM,MIN    !Get ID of the first elem
*CFOPEN,mesh_nodes,inp
*GET,node_,NODE,0,NUM,MIN    !Get ID of the first node
*DO,i,1,num_nodes,1
  *GET,coordx_,NODE,node_,LOC,X
  *GET,coordy_,NODE,node_,LOC,Y
  *GET,coordz_,NODE,node_,LOC,Z 
  *VWRITE,'n,',i,',',coordx_,',',coordy_,',',coordz_
%C%I%C%G%C%G%C%G
  *GET,node_,NODE,node_,NXTH   ! select the next node  
*ENDDO
*CFCLOSE
!Write nodes conectivity
*CFOPEN,mesh_elements,inp
*GET,num_elem,ELEM,0,COUNT
*GET,elem_,ELEM,0,NUM,MIN    !Get ID of the first element
*DO,i,1,num_elem,1
  *GET,vertice_1,ELEM,elem_,NODE,1
  *GET,vertice_2,ELEM,elem_,NODE,2
  *GET,vertice_3,ELEM,elem_,NODE,3
  *GET,vertice_4,ELEM,elem_,NODE,4  
  *VWRITE,'en',i,vertice_1,vertice_2,vertice_3,vertice_4
%C,%I,%I,%I,%I,%I
  *GET,elem_,ELEM,elem_,NXTH  ! select the next element
*ENDDO
*CFCLOSE
CDWRITE,DB,file,cdb\n""")
               