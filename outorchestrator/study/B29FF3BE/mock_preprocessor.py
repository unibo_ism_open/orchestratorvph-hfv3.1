import os

class Preprocessor:

    def __init__(self):

        self.outputs = ["i_cellularity.txt", "i_vascularization.txt", "mesh_elements.inp", "mesh_nodes.inp", "drug_regime.csv", "drugs.is_active.tsv"]
        self.path = '../../vph-hfv3-4.0/initial_input/B29FF3BE/'

    def main(self):

        for out in self.outputs:
            cmd = f"cp {self.path}{out} {out}"
            os.system(cmd)

if __name__ == "__main__":

    m = Preprocessor()
    m.main()