# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 12:05:53 2022

@author: Diego
"""

import numpy as np
from scipy.interpolate import griddata

with open(r"mesh_nodes.inp", 'r') as fp:
    nodes = len(fp.readlines())
with open(r"mesh_elements.inp", 'r') as fp:
    elements = len(fp.readlines())
 
coords = np.genfromtxt(r'mesh_nodes.inp', max_rows=nodes, delimiter=',', usecols = [1,2,3,4])
connect=np.genfromtxt(r'mesh_elements.inp', max_rows=elements, delimiter=',', usecols = [2,3,4,5]).astype(int)

centroid_elems = np.empty((elements,3))
for i in range(centroid_elems.shape[0]):
    centroid_elems[i,:] = (coords[coords[:,0]==connect[i,0]][:,1:] + coords[coords[:,0]==connect[i,1]][:,1:] + coords[coords[:,0]==connect[i,2]][:,1:] + coords[coords[:,0]==connect[i,3]][:,1:])/4

elemental_ktrans = np.loadtxt('i_vascularization.txt')[:,1:]
elemental_cell = np.loadtxt('i_cellularity.txt')[:,1:]

interp_ktrans = griddata(centroid_elems, elemental_ktrans, coords[:,1:], method = 'nearest')
interp_cell = griddata(centroid_elems, elemental_cell, coords[:,1:], method = 'nearest')

np.savetxt(r'i_cellularity_nodal.txt', np.append(np.array(range(1,interp_cell.shape[0]+1),int).reshape(-1,1), interp_cell.reshape(-1,1), axis=1), fmt=['%i', '%10.7f'])
np.savetxt(r'i_vascularization_nodal.txt', np.append(np.array(range(1,interp_ktrans.shape[0]+1),int).reshape(-1,1), interp_ktrans.reshape(-1,1), axis=1), fmt=['%i', '%10.7f'])

