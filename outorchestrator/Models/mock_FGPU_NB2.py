import sys
import random
import time
import json

class Model:

	def __init__(self, number):

		self.file2 = "output_"+str(number)
		self.path = '../../../'


	def main(self,ids):

		## Creates an id list
		idslist = ids.split(" ")
		file2 = open(self.file2, 'w')

		for i in range(len(idslist)-1):

		## Create output files
			inputname = open("input-"+idslist[i]+".json",'r')
			inputdt = json.load(inputname)
			self.file1 = "output-"+idslist[i]+".json"
			outputfile = open(self.file1,'w')
			file1 = open(self.file1, 'w')

			iopath = self.path+'/vph-hfv3-3.0/Models_examples/abm/'

			## Open output to be copied
			output = open(iopath+"USFD_sample_output.json",'r')

			## Read data to be copied
			dt = json.load(output)
			dt['primage']['O2'] = inputdt['primage']['environment']['O2'] + 0.1
			dt['primage']["tumour_volume"] = inputdt['primage']['environment']['V_tumour']+0.05
			dt['primage']["total_volume_ratio_updated"] = inputdt['primage']['environment']['V_tumour']/dt['primage']["tumour_volume"]
			dt['primage']['cellularity'] = [random.randrange(0,1), random.randrange(0,1), random.randrange(0,1), random.randrange(0,1), random.randrange(0,1), random.randrange(0,1)]
			
			json.dump(dt, outputfile)
			file1.close()

		duration = 0.0001#random.randrange(0,1)
		cpu_start = time.time() 
		totaltime = 0
		while totaltime < duration:
			file2.write("writing something\n")
			time.sleep(0.1)
			totaltime = time.time()-cpu_start

		## Write log info
		file2.write("Wed Feb 24 16:45:04 2021 \n+-----------------------------------------------------------------------------+| NVIDIA-SMI 440.33.01    Driver Version: 440.33.01    CUDA Version: 10.2     |\n|-------------------------------+----------------------+----------------------+\n| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |\n| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |\n|===============================+======================+======================|\n|   0  Tesla K40d          On   | 00000000:81:00.0 Off |                    0 |\n| N/A   39C    P8    15W / 300W |      0MiB / 11441MiB |      0%      Default |\n+-------------------------------+----------------------+----------------------+\n\n+-----------------------------------------------------------------------------+\n| Processes:                                                       GPU Memory |\n|  GPU       PID   Type   Process name                             Usage      |\n|=============================================================================|\n|  No running processes found                                                 |\n+-----------------------------------------------------------------------------+\nGPU 0: Tesla K40d, SM37, Linux, pciBusId 129\n64Bit System Detected\nAllocating Host and Device memory\nNo initial states file specified. Using default values.\nRandom seed: 880567\nGenerated random agent population. NB: 6786, SC: 6786\n"+
ids+"\nDone.")
		file2.close()


if __name__ == "__main__":

	m = Model(sys.argv[2])
	m.main(str(sys.argv[1]))


