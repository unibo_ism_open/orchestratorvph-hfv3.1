# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 15:11:33 2020

@author: Silvia Hervas-Raluy
Latest changes: 14/Nov/2022
"""

import numpy as np

e_cell = 0.08e-3
e_ecm = 0.8e-3
poisson = 0.38
alpha = 0.333

# Transform volume updated into ansys format
vol = np.genfromtxt('updated_volume.txt') - 1
num_elems = len(vol)
# Stablish a manual threshold to avoid extreme values
min_vol_ratio = -0.7
vol[vol < min_vol_ratio] = min_vol_ratio

fname="growth_APDL.txt"
with open(fname, 'w') as file:
    for i in range (0,num_elems * 2):
        if i < num_elems:
            ii = i
        else:
            ii = i - num_elems
        file.write('BFE,%i,TEMP,,%.3f\n' % (i+1,vol[ii]))
        

# Transform cellularity into material properties  
# updated_cellularity has 6 columns: 
# [1]: alive NB vol fraction, [2]: alive SC vol fraction
# [3]: apoptotic NB vol fraction, [4]: necrotic NB vol fraction, [5]: apoptotic SC vol fraction, [6]: necrotic SC vol fraction
cell_fractions = np.genfromtxt('updated_cellularity.txt')
cell_alive = cell_fractions[:, [0, 1]] # extract alive cell fractions
cell = cell_alive.sum(axis=1) # sum both NB and SC volume fractions
# Ensure a minimum value of stiffness if there are no cells
min_cellularity = 0.1
cell[cell < min_cellularity] = min_cellularity
e_celula = e_cell*cell
e_matriz = e_ecm*(1-cell)
elems = np.genfromtxt('mesh_elements.inp',delimiter=',',usecols=(1,2,3,4,5))
# Write material properties file in ANSYS format
f = open('materials_elastic.inp', 'w')
for i in range (0,len(e_celula)):
    f.write('MP,EX,%i,%.5e\nMP,EX,%i,%.5e\nMP,NUXY,%i,%.3f\nMP,NUXY,%i,%.3f\nMP,ALPX,%i,%.5e,\nMP,ALPX,%i,%.5e,\n' \
    % (i+1,e_celula[i],i+1+num_elems,e_matriz[i],i+1,poisson,i+1+num_elems,poisson,i+1,alpha,i+1+num_elems,alpha))
f.close()

#Write mesh file for structural analysis (different from diffusion)
f = open('mesh_structural.inp', 'w')
np.savetxt(f, elems,fmt='en,%i,%i,%i,%i,%i')
elems[:,0] = elems[:,0] + num_elems
np.savetxt(f, elems,fmt='en,%i,%i,%i,%i,%i')
f.close()

